# FreedomBox in a Container

Docker scripts that install and run Freedombox in a container.

## Installation

  - First install `ds` and `revproxy`:
     + https://gitlab.com/docker-scripts/ds#installation
     + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull freedombox`

  - Create a directory for the container: `ds init freedombox @freedombox`

  - Fix the settings: `cd /var/ds/freedombox/ ; vim settings.sh`

  - Build image, create the container and configure it: `ds make`

  - If the domain is not a real one, add to `/etc/hosts` the line:
    `127.0.0.1 freedombox.example.org`

  - Now you can access the website at: https://freedombox.example.org

## Maintenance

    ```
    ds stop
    ds start
    ds shell
    ds help

    # To-Do
    ds backup
    ds restore
    ds remake

    ds update
    ds upgrade
    ```